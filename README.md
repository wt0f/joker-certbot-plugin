Joker.com certbot plugin
========================
[![pipeline status](https://gitlab.com/wt0f/joker-certbot-plugin/badges/master/pipeline.svg)](https://gitlab.com/wt0f/joker-certbot-plugin/commits/master) [![coverage report](https://gitlab.com/wt0f/joker-certbot-plugin/badges/master/coverage.svg)](https://gitlab.com/wt0f/joker-certbot-plugin/commits/master)

This plugin allow one to create the DNS TXT records for Let's Encrypt
authentication. It unfortunately uses Joker's janky
[POST call](https://joker.com/faq/content/6/496/en/let_s-encrypt-support.html)
instead of a real API, but it does work.

Joker's view of only giving API access to reseller accounts is quite
antiquated and my suggestion that they allow all users to have access to
the API have all been rejected. If you feel the same, please submit a
support ticket and voice your opinion. Maybe they will change their
policies in the future.

Unfortunately, I really can not recommend them for a Registrar any longer
and will be busy moving domains to another Registrar where I can have a
real API and not use their janky approach. But until then I have created
this plugin to keep myself going and with luck help out others that are
stuck with using Joker.

Installation
------------
Installation is quite simple. Do a clone of this repository and run the
setup tools install.

    git clone https://gitlab.com/wt0f/joker-certbot-plugin.git
    cd joker-certbot-plugin
    python setup.py install

At that point you should be able to see the plugin listed with
`certbot plugins`. Of course you need to already have `certbot`
installed.

Usage
-----
You need to activate the plugin by adding `-a joker:dns` to the `certbot`
command line. For example here is a typical usage:

    certbot certonly --agree-tos -a certbot_joker:dns -d <HOSTNAME> --certbot_joker:dns-username <USERNAME> --certbot_joker:dns-password <PASSWORD>

The <USERNAME> and <PASSWORD> arguments are the Dyn-DNS username and password
that is found on your DNS control panel. If you have not enabled Dyn-DNS, then
you will need to have it enabled to use the plugin.

Arguments
---------
The following table provides the command line switches that are accepted.

| Argument                 | Default Value | Description         |
|-------------------------------|------|-------------------------|
| joker:dns-username            | None | Dyn-DNS username        |
| joker:dns-password            | None | Dyn-DNS password        |
| joker:dns-zone                | None | Domain to create record |
| joker:dns-propagation-seconds |  45  | Secs to wait for DNS    |

For the `cli.ini` file, one just removes `joker:dns-` from the argument
to configure the entries in `cli.ini`.

If the `zone` argument is not supplied, the plugin does attempt to auto-detect
the correct DNS zone to make the record in. Effectively what is passed in as
the hostname (`-d` option) the last two components of the hostname will be
used as the `zone` argument. This can always be overridden by specify the
`zone`.

