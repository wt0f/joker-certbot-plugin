#!/usr/bin/env python

"""Example Certbot plugins.
For full examples, see `certbot.plugins`.
"""
import logging
import zope.interface
import requests
import pprint

from certbot import errors
from certbot import interfaces
from certbot.plugins import dns_common

logger = logging.getLogger(__name__)


@zope.interface.implementer(interfaces.IAuthenticator)
@zope.interface.provider(interfaces.IPluginFactory)
class Authenticator(dns_common.DNSAuthenticator):
    """Joker.com Authenticator."""

    description = 'Obtain certs using a TXT record using the Joker.com API.'

    joker_post_endpoint = 'https://svc.joker.com/nic/replace'

    def __init__(self, *args, **kwargs):
        super(Authenticator, self).__init__(*args, **kwargs)

        # Joker DNS takes a bit longer to propagate
        setattr(self.config, self.dest('propagation-seconds'), 45)

    @classmethod
    def add_parser_arguments(cls, add):  # pylint: disable=arguments-differ
        super(Authenticator, cls).add_parser_arguments(add)
        add('username', help='Joker DynDNS username.')
        add('password', help='Joker DynDNS password.')
        # add('api-key', help='Joker API key.')
        add('zone', default=None, help='DNS zone to create the record')

    def more_info(self):  # pylint: disable=missing-docstring,no-self-use
        return 'This plugin configures a DNS TXT record to respond to a ' + \
               'dns-01 challenge using the Joker.com API.'

    def _setup_credentials(self):
        return

    def _perform(self, domain, validation_name, validation):
        zone = self.conf('zone')
        if zone is None:
            # attempt to automagically determine zone
            # (i.e save the last 2 components)
            zone = '.'.join((domain.split('.'))[-2:])

        # Joker.com has the annoying habit of adding the domain on to
        # what is provided as the TXT record. ARG!
        label = validation_name.replace(".%s" % zone, '')

        # build the parameters to send to Joker.com POST
        params = {'username': self.conf('username'),
                  'password': self.conf('password'),
                  'zone': zone,
                  'label': label,
                  'type': 'TXT',
                  'value': validation,
                  'ttl': 60,
                  }

        logger.info("joker:dns creating TXT record")
        logger.debug("joker:dns %s => %s" % (params['label'], params['value']))
        r = requests.post(self.joker_post_endpoint, data=params)

        if r.text.startswith('badauth'):
            logger.error("joker:dns Unable to authenticate to Joker. "
                         "Please check credentials")
            raise errors.AuthorizationError()
        elif r.text.startswith('OK:'):
            logger.info("joker:dns Joker.com reports back that TXT "
                        "record created")
        elif r.text.startswith('Error:'):
            logger.error("joker.dns Unknown error: %s" % r.text)
            params['password'] = '*REDACTED*'
            logger.error("parameters = %s" % pprint.pformat(params))
        else:
            logger.warning("joker:dns Unknown response: %s" % r.text)

    def _cleanup(self, domain, validation_name, validation):
        zone = self.conf('zone')
        if zone is None:
            # attempt to automagically determine zone
            # (i.e save the last 2 components)
            zone = '.'.join((domain.split('.'))[-2:])

        # Joker.com has the annoying habit of adding the domain on to
        # what is provided as the TXT record. ARG!
        label = validation_name.replace(".%s" % zone, '')

        params = {'username': self.conf('username'),
                  'password': self.conf('password'),
                  'zone': zone,
                  'label': label,
                  'type': 'TXT',
                  'value': "",
                  }

        logger.info("joker:dns removing TXT record")
        r = requests.post(self.joker_post_endpoint, data=params)

        if r.text.startswith('badauth'):
            logger.error("joker:dns Unable to authenticate to Joker. "
                         "Please check credentials")
            raise errors.AuthorizationError()
        elif r.text.startswith('OK:'):
            logger.info("joker:dns Joker.com reports back that TXT "
                        "record removed")
        elif r.text.startswith('Error:'):
            logger.error("joker.dns Unknown error: %s" % r.text)
            params['password'] = '*REDACTED*'
            logger.error("parameters = %s" % pprint.pformat(params))
        else:
            logger.warning("joker:dns Unknown response: %s" % r.text)
