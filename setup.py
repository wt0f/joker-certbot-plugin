from setuptools import setup
from setuptools import find_packages

setup(
    name='certbot_joker',
    description="Joker.com plugin for Let's Encrypt client",
    url="https://gitlab.com/wt0f/joker-certbot-plugin",
    author="Gerard Hickey",
    author_email="hickey@kinetic-compute.com",
    packages=find_packages(),
    install_requires=[
        'acme',
        'certbot',
        'zope.interface',
        'requests',
    ],
    entry_points={
        'certbot.plugins': [
            'certbot_joker:dns = certbot_joker.plugin:Authenticator',
        ],
    },
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
)
